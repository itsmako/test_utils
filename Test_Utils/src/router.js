import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import RequestGenerator from './views/RequestGenerator.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      props: { default: true, sidebar: false },
    },
    {
      path: '/generator',
      name: 'generator',
      component: RequestGenerator,
      props: { default: true, sidebar: false },
    },
  ],
});
