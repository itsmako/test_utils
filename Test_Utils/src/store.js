/* eslint-disable */
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    intervalActive: true,
    currentView: null,
    currentViewName: null,
    queryBuilderData: {
      mark: true,
      fields: [],
      groups: [
        {
          mark: true,
          fields: [],
          groups: [],
        },
        {
          mark: false,
          fields: [
            {
              mark: true,
              field: 'field1',
              type: '>=',
              value: '1234543543',
            },
            {
              mark: true,
              field: 'field2',
              type: '<',
              value: '1234543543',
            },
          ],
          groups: [],
        },
      ],
    },
    fieldsList: [
      {
        name:'field1',
        type:'SqlInput'
      },
      {
        name:'field2',
        type:'SqlInput'
      },
      {
        name:'field3',
        type:'SqlInput'
      }
    ],
    expressionList:[
      {
        name:'<',
      },
      {
        name:'>',
      },
      {
        name:'=<',
      },
      {
        name:'=>',
      },
      {
        name:'<>',
      }
      ,
      {
        name:'LIKE',
      }
    ]
  },
  getters: {
    getIntervalActive: state => () => state.intervalActive,
    getCurrentView: state => () => state.currentView,
    getCurrentViewName: state => () => state.currentViewName,
    GetQueryBuilderData: state => () => state.queryBuilderData,
    GetFieldList: state => () => state.fieldsList,
    GetExpressionList: state => () => state.expressionList,
  },
  mutations: {
    stopStoreInterval: (state) => {
      state.intervalActive = false;
    },
    startStoreInterval: (state) => {
      state.intervalActive = true;
    },
    setCurrentViewID: (state, newView) => {
      state.currentView = newView;
    },
    setCurrentViewName: (state, newViewName) => {
      state.currentViewName = newViewName;
    },
  },
  actions: {

  },
});
